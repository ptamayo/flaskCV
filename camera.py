import cv2
import os
haarcascades_eye_path = os.path.dirname(cv2.__file__) + "/data/haarcascade_eye.xml"        
haarcascades_path = os.path.dirname(cv2.__file__) + "/data/haarcascade_frontalface_default.xml"

class VideoCamera(object):
    def __init__(self):
        self.face_cascade = cv2.CascadeClassifier(haarcascades_path)
        self.eye_cascade = cv2.CascadeClassifier(haarcascades_eye_path)
        self.video = cv2.VideoCapture(0)
        self.ninja = cv2.imread('static/ninja_mod.jpg',-1)
        
    def __del__(self):
        self.video.release()
    
    def get_frame(self):
        success, image = self.video.read()
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        faces = self.face_cascade.detectMultiScale(
            gray,
            scaleFactor=1.1,
            minNeighbors=5,
            minSize=(30, 30),
            flags=cv2.CASCADE_SCALE_IMAGE
        )

        for (x, y, w, h) in faces:
            rows,cols,channels = self.ninja.shape
            roi = image[y-20:rows+y-20, x-115:cols+x-115]
            ninja_gray = cv2.cvtColor(self.ninja,cv2.COLOR_BGR2GRAY)
            ret, mask = cv2.threshold(ninja_gray, 100, 255, cv2.THRESH_BINARY_INV)
            mask_inv = cv2.bitwise_not(mask)
            try:
                image_bg = cv2.bitwise_and(roi,roi,mask = mask_inv)
                ninja_fg = cv2.bitwise_and(self.ninja,self.ninja,mask = mask)
                dst = cv2.add(image_bg,ninja_fg)
                image[y-20:rows+y-20, x-115:cols+x-115] = dst
            except:
                print("Error when drawing image")


        # Additional Eye Tracking
        # eyes = self.eye_cascade.detectMultiScale(
        #     gray,
        #     scaleFactor=1.1,
        #     minNeighbors=5,
        #     minSize=(20,20),
        #     flags = cv2.CASCADE_SCALE_IMAGE
        # )

        # for (x, y, w, h) in eyes:
        #     cv2.rectangle(image, (x, y), (x+w, y+h), (0, 255, 0), 2)

        
        ret, jpeg = cv2.imencode('.jpg', image)
        return jpeg.tobytes()